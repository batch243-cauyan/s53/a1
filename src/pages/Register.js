import { useState, useEffect, useSyncExternalStore } from 'react';
import {Button, Form, Col, Row, Container} from 'react-bootstrap';


const Register = () => {

    // Statehooks
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);

    useEffect(()=>{
        if (email && password1 && password2 && password1===password2){
            setIsActive(true);
        }else{
            setIsActive(false)
    }

    console.log(email);
    console.log(password1);
    console.log(password2);

    },[email,password1,password2])

    function registerUser(event){
        event.preventDefault();
        alert(`Congratulations ${email} you are now registered in our website!`);
        setEmail('');
        setPassword1('');
        setPassword2('');
    }


    return (
        <Container>
        <Row>
            <Col className='offset-4 mt-4' xs={12} md={6} lg={4}>
                <Form className='bg-secondary p-3' onSubmit={registerUser}>
                    <Form.Group className="mb-3 " controlId="email">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" required value={email} onChange={event =>{
                            setEmail(event.target.value)
                        }}/>
                        <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password1">
                        <Form.Label>Enter your desired Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" required value={password1} onChange={event =>{
                            setPassword1(event.target.value)
                        }}/>

                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password2">
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" required value={password2} onChange={event =>{
                            setPassword2(event.target.value)
                        }}/>
                    </Form.Group>
                   <div className='text-center'>
                    <Button className ="btn-lg"variant="primary" type="submit" disabled={!isActive} >
                        Register
                    </Button>
                    </div>
                </Form>
        </Col>
        </Row>
        </Container> 
  );
}

 
export default Register;