import { Nav,A } from "react-bootstrap";
import { NavLink } from "react-router-dom";



const Error = () => {
    return ( 
        <div>
            <h1>Page Not Found</h1>
            <p>Go back to the <a href="/">homepage</a></p>
        </div>
     );
}
 
export default Error;