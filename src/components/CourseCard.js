import {useEffect, useState} from 'react';
import {Row, Col, Button, Card} from 'react-bootstrap'

export default function CourseCard(prop){
	console.log(prop);

	//destructuring the courseProp that were passed from the Course.js
	/*const {name, description, price} = courseProp;*/
	const {id, name,description, price, slots} =prop.prop;

	//Use the state hook for this component to be able to store its state specifically to monitor the number of enrollees
	//States are used to keep track information related to individual components
	//Syntax:
		// const [getter, setter] =useState(initialGetterValue)

	const [enrollees, setEnrollees] = useState(0);
	const [slotsAvailable, setSlotsAvailable] = useState(slots);
	const [isAvailable, setIsAvailable] = useState(true);

	// Add a useEffect hook to have "CourseCard" component to perform a certain task after every DOM update
		// Syntax: useEffect(functionToBeTriggered, [statesToBeMonitored])

	useEffect(()=>{
		if(slotsAvailable===0){
			
			setIsAvailable(false);
		}
	}, [slotsAvailable, enrollees])

	/*setEnrollees(1)*/

	function enroll(){
		// My code
		// if(slotsAvailable>0){
		// 	setEnrollees(enrollees+1);
		// 	setSlotsAvailable(slotsAvailable-1);
		// }
		// else{
		// 	// document.getElementById(`btn${id}`).disabled = true;
		// 	alert('No more seats.')
			
		// }

		if(slotsAvailable===1){
			alert("Congrats, you were able to enroll before the cut!")
		}
		setEnrollees(enrollees+1);
			setSlotsAvailable(slotsAvailable-1);
		
	}
	

	// since enrollees is declare as a constant variable, directly reassigning the value is not allowed or will cause an error
	/*enrollees = 1;
	console.log(enrollees);*/

	
	return(
		<Row >
			<Col xs = {12} md = {4} className = "offset-md-4 offset-0">
				<Card>
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>

				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>
				         	{description}
				        </Card.Text>

				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>
				         	P {price}
				        </Card.Text>

				        <Card.Subtitle>Enrollees</Card.Subtitle>
				        <Card.Text>
				         	{enrollees}
				        </Card.Text>

				        <Card.Subtitle>Slots Available:</Card.Subtitle>
				        <Card.Text>
				         	{slotsAvailable} slots
				        </Card.Text>

						<Button id={`btn${id}`} variant="primary" onClick = {enroll} disabled ={!isAvailable} >Enroll</Button>

						{/* My Code using Id to disable */}
				        {/* <Button id={`btn${id}`} variant="primary" onClick = {enroll} >Enroll</Button> */}
				      </Card.Body>
				    </Card>
			</Col>
		</Row>

		)
}