/*import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';*/

// importing modules using deconstruct
import { Fragment, useState,useEffect} from 'react';
import {Container, Nav, Navbar} from 'react-bootstrap';

import { NavLink } from 'react-router-dom';


export default function AppNavbar(){

	// create a state to store the user information stored in the local storage from the login page

	const [user,setUser] = useState(localStorage.getItem('email'));
	console.log(user);
	useEffect(()=>{
        setUser(localStorage.getItem("email"))
    }, [user])

	return(
		<Navbar bg="light" expand="lg" className='vw-100'>
		      <Container fluid>
		        <Navbar.Brand as = {NavLink} to="/">Course Booking</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		            <Nav.Link as = {NavLink} to="/" >Home</Nav.Link>
		            <Nav.Link as = {NavLink} to="/courses">Courses</Nav.Link>
					
					{
					  (user) ? 
					  <Nav.Link as = {NavLink} to="/logout">Logout</Nav.Link>
					  :
					  <Fragment>
						<Nav.Link as = {NavLink} to="/register">Register</Nav.Link>
						<Nav.Link as = {NavLink} to="/login">Login</Nav.Link>
					  </Fragment>
					}


		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
		)
}