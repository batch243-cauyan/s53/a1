
// import {Fragment} from 'react';
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';


function App() {

  /*const name = 'John Smith';
  const element = <h1>Hello, {name}</h1>;*/


  return (
    <Router>
      <AppNavbar/>
      <Routes>
        <Route  path="/" element ={<Home/>}/>
        <Route  path="/courses" element ={<Courses/>}/>
        <Route  path="/register" element ={<Register/>}/>
        <Route  path="/login" element ={ <Login/>}/>
        <Route  path="/logout" element ={ <Logout/>}/>
        <Route  path="/:route" element ={ <Error/>}/>
      </Routes>
    </Router>
      
      
      
  
     

  );
}

export default App;
